package com.sample.app.request;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.sample.app.utils.Common;

import org.json.JSONObject;

/**
 * Created by zdref on 2/8/18.
 */

public class MovieRequestTask extends AbstractRequestTask {

    public MovieRequestTask(Context context) {
        super(context);
    }

    @Override
    public String getUrl() {
        return Common.DATA_URL;
    }

    @Override
    public String getName() {
        return "Retrive Movies";
    }

    @Override
    public String getProgressText() {
        return "Retrieving movies...";
    }

    @Override
    JSONObject getJSONParams() {
        return null;
    }

    @Override
    void handleResponse(JSONObject response) {
        taskListener.handleResponseCallback(response);
    }

    @Override
    void handleErrorResponse(VolleyError error) {
        taskListener.handleErrorResponeCallback(error);
    }

    @Override
    public int getRequestMethod() {
        return Request.Method.GET;
    }
}
