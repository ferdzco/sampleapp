package com.sample.app.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.sample.app.ApplicationManager;
import com.sample.app.R;
import com.sample.app.model.Movie;
import com.sample.app.ui.fragments.MovieDetailFragment;
import com.sample.app.ui.fragments.MovieListFragment;


public class MovieActivity extends AppCompatActivity implements MovieListFragment.OnItemSelectedListener {

    private static final String TAG = MovieActivity.class.getSimpleName();
    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 123;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        //permission handling
        String[] permissions = {Manifest.permission.INTERNET,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};

        if(!hasPermissions(this, permissions)){
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_PERMISSION_MULTIPLE);
            return;
        }


        if (getResources().getBoolean(R.bool.landscapeMode)) {
            return;
        }else{
            if (savedInstanceState != null) {
                getSupportFragmentManager().executePendingTransactions();
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                if (fragment != null) {
                    getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                }
            }

            MovieListFragment movieListFragment = new MovieListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, movieListFragment)
                    .commit();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_MULTIPLE: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //create directory
                    ApplicationManager.getInstance().createAppDriectoryAndFileNoMedia();

                    MovieListFragment movieListFragment = new MovieListFragment();
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.container, movieListFragment)
                            .commit();
                }
                return;
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onItemSelected(Movie movie) {
        boolean isLandscape = getResources().getBoolean(R.bool.landscapeMode);

        int container = R.id.container;
        Fragment fragment = null;

        Bundle bundle = new Bundle();
        bundle.putSerializable("movie", movie);

        if(isLandscape){
            MovieDetailFragment movieDetailFragment = new MovieDetailFragment();
            movieDetailFragment.setArguments(bundle);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.detailContainer, movieDetailFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }else{
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }


    }


    public static boolean hasPermissions(Context context, String... permissions){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null){
            for(String permission: permissions){
                if(ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED){
                    return false;
                }
            }
        }
        return true;
    }
}
