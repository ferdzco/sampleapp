package com.sample.app.ui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.VolleyError;
import com.sample.app.ApplicationManager;
import com.sample.app.R;
import com.sample.app.model.Movie;
import com.sample.app.request.MovieRequestTask;
import com.sample.app.request.RequestTaskListener;
import com.sample.app.ui.adapters.MovieAdapter;
import com.sample.app.utils.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MovieListFragment extends Fragment implements RequestTaskListener, AdapterView.OnItemClickListener {

    private static final String TAG = MovieListFragment.class.getSimpleName();

    private OnItemSelectedListener itemSelectedListener;
    private ProgressDialog progressDialog;

    private ListView mMovieListView;
    private MovieAdapter adapter;
    private ArrayList<Movie> movieList = new ArrayList<Movie>();



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("ApplicationManager",  "onCreate MovieListFragment");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_list, container, false);

        mMovieListView = (ListView) view.findViewById(R.id.listOfMovies);
        mMovieListView.setOnItemClickListener(this);
        adapter = new MovieAdapter(getContext(), movieList);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach()");
        if (context instanceof OnItemSelectedListener) {
            itemSelectedListener = (OnItemSelectedListener) context;

            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.show();

            MovieRequestTask movieRequestTask = new MovieRequestTask(context);
            movieRequestTask.setTaskListener(this);
            movieRequestTask.executeRequest();
        } else {
            throw new ClassCastException(context.toString()
                    + " Must implement MovieListFragment.OnItemSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void handleResponseCallback(JSONObject jsonObject) {
        Log.d(TAG, "response :: "+ jsonObject.toString());

        try {
            String stat = jsonObject.getString("status");
            Log.d(TAG, "stat :: "+ stat);
            if("OK".equalsIgnoreCase(stat)){
                JSONObject dataObj = jsonObject.getJSONObject("data");
                JSONArray moviesObjArr = dataObj.getJSONArray("movies");

                for(int i = 0; i < moviesObjArr.length(); i++){
                    Movie movie = new Movie();
                    JSONObject movieObj =  moviesObjArr.getJSONObject(i);
                    movie.setId(movieObj.getInt("id"));
                    movie.setRating(movieObj.getString("rating"));
                    movie.setLanguage(movieObj.getString("language"));
                    movie.setTitle(movieObj.getString("title"));
                    movie.setUrl(movieObj.getString("url"));
                    movie.setTitleLong(movieObj.getString("title_long"));
                    movie.setImdbCode(movieObj.getString("imdb_code"));
                    movie.setState(movieObj.getString("state"));
                    movie.setYear(movieObj.getString("year"));
                    movie.setRuntime(movieObj.getInt("runtime"));
                    movie.setOverview(movieObj.getString("overview"));
                    movie.setMpaRating(movieObj.getString("mpa_rating"));
                    movie.setSlug(movieObj.getString("slug"));

                    // for genres
                    JSONArray genresObjArr = movieObj.getJSONArray("genres");
                    String[] genres = new String[genresObjArr.length()];
                    for(int j = 0; j < genresObjArr.length(); j++){
                        genres[j] = genresObjArr.getString(j);
                    }
                    movie.setGenres(genres);

                    // downlaod images
                    downloadImages(movieObj.getString("slug"));

                    movieList.add(movie);
                }
                mMovieListView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                mMovieListView.setSelection(0);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        progressDialog.dismiss();

        if(getResources().getBoolean(R.bool.landscapeMode)){
            itemSelectedListener.onItemSelected(movieList.get(0));
        }
    }

    private void downloadImages(String slugFileName){
        // for cover
        ApplicationManager.getInstance().downloadImage(String.format(Common.COVER_IMAGE_URL, slugFileName), slugFileName+"-cover");
        //for backdrop
        ApplicationManager.getInstance().downloadImage(String.format(Common.BACKDROP_IMAGE_URL, slugFileName), slugFileName+"-backdrop");
    }

    @Override
    public void handleErrorResponeCallback(VolleyError error) {
        progressDialog.dismiss();
    }

    @Override
    public void updateProgress(String text) {
        progressDialog.setMessage(text);

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Movie selectedMovie = (Movie) adapterView.getItemAtPosition(position);
        itemSelectedListener.onItemSelected(selectedMovie);
    }

    public interface OnItemSelectedListener {
        void onItemSelected(Movie movie);
    }
}
