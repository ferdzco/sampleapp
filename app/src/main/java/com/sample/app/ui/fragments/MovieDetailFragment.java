package com.sample.app.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sample.app.R;
import com.sample.app.model.Movie;
import com.sample.app.utils.ImageUtil;


public class MovieDetailFragment extends Fragment {

    private static final String TAG = MovieDetailFragment.class.getSimpleName();

    private TextView mTitle;
    private TextView mYearRelease;
    private TextView mRatings;
    private TextView mOverview;
    private ImageView mBackdrop;
    private ImageView mCover;


    public MovieDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_detail, container, false);

        mTitle = (TextView) view.findViewById(R.id.movieTitle);
        mYearRelease = (TextView) view.findViewById(R.id.yearRelease);
        mRatings = (TextView) view.findViewById(R.id.ratings);
        mOverview = (TextView) view.findViewById(R.id.overview);
        mBackdrop = (ImageView) view.findViewById(R.id.backdrop);
        mCover = (ImageView) view.findViewById(R.id.cover);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            Movie movie = (Movie) bundle.getSerializable("movie");
            populatedView(movie);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void populatedView(Movie movie){
        mTitle.setText(movie.getTitle());
        mYearRelease.setText(movie.getYear());
        mRatings.setText(movie.getRating());
        mOverview.setText(movie.getOverview());

        mCover.setImageBitmap(ImageUtil.getImageBitmapThumbnail(Environment.getExternalStorageDirectory().toString()
                .concat("/sampleApp/")+movie.getSlug()+"-cover.jpg"));

        mBackdrop.setImageBitmap(ImageUtil.getImageBitmapThumbnail(Environment.getExternalStorageDirectory().toString()
                .concat("/sampleApp/")+movie.getSlug()+"-backdrop.jpg"));
    }
}
