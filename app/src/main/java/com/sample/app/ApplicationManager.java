package com.sample.app;

import android.app.Application;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

import com.sample.app.service.FileDownloaderService;
import com.sample.app.utils.volley.MyVolley;

import java.io.File;
import java.io.IOException;

/**
 * Created by zdref on 2/8/18.
 */

public class ApplicationManager extends Application {

    public static ApplicationManager instance;

    public static final String DIR_EXTERNAL_STORAGE = Environment.getExternalStorageDirectory().toString();
    public static final String DIR_SAMPLE_APP = "/sampleApp/";
    public static final String DIR_SAMPLE_APP_EXTERNAL_STORAGE =  DIR_EXTERNAL_STORAGE.concat(DIR_SAMPLE_APP);

    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationManager.init(this);
        init();
    }

    public static ApplicationManager getInstance(){
        return instance;
    }

    private static void init(final ApplicationManager applicationManager) {
        instance = applicationManager;
    }

    private void init(){
        MyVolley.init(this);
    }


    public void createAppDriectoryAndFileNoMedia(){
        if(!isDirectoryExists(DIR_SAMPLE_APP_EXTERNAL_STORAGE)){
            createDirectory(DIR_EXTERNAL_STORAGE, DIR_SAMPLE_APP);
            createNoMediaFile(DIR_SAMPLE_APP_EXTERNAL_STORAGE, ".nomedia");
        }
    }

    private boolean isDirectoryExists(String path){
        boolean isExists = false;
        File newFile = new File(path);
        if(newFile.exists()){
            isExists = true;
        }
        return isExists;
    }


    /**
     * create directory if not exist.
     * directory for profile picture of each contact
     * @param filePath
     * @param folderName
     */
    private void createDirectory(String filePath, String folderName){
        File newFolder = new File(filePath, folderName);
        boolean isCreated = newFolder.mkdirs();
        Log.d("App", "Is Directory Created :: "+ isCreated);
    }

    /**
     * Method that create .nomedia file.
     *
     * @param filePath
     * @param folderName
     */
    private void createNoMediaFile(String filePath, String folderName){
        File newFile = new File(filePath, folderName);
        boolean isCreated = false;
        try{
            isCreated = newFile.createNewFile();
            Log.d("App", "Is File Created :: "+ isCreated);
        }catch(IOException io){
            Log.d("App","IOException : "+ io);
        }
    }

    public void downloadImage(String url,String fileId){
        Intent fileDownloadIntent = new Intent(this, FileDownloaderService.class);
        fileDownloadIntent.putExtra("url", url);
        fileDownloadIntent.putExtra("fileID", fileId);
        startService(fileDownloadIntent);
    }

}
