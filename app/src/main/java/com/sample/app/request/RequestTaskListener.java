package com.sample.app.request;


import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface RequestTaskListener {
    void handleResponseCallback(JSONObject jsonObject);
    void handleErrorResponeCallback(VolleyError error);
    void updateProgress(String text);
}
