package com.sample.app.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.sample.app.ApplicationManager;
import com.sample.app.utils.volley.InputStreamVolleyRequest;
import com.sample.app.utils.volley.MyVolley;

import java.io.File;
import java.io.FileOutputStream;


public class FileDownloaderService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String fileId = intent.getStringExtra("fileID");
        String url = intent.getStringExtra("url");
        wtfDownload(url, fileId+".jpg");

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void wtfDownload(String url, final String name){
        InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, url,
                new Response.Listener<byte[]>() {
                    @Override
                    public void onResponse(byte[] response) {
                        // TODO handle the response
                        try {
                            if (response!=null) {

                                FileOutputStream outputStream;
                                outputStream = new FileOutputStream(new File(ApplicationManager.DIR_SAMPLE_APP_EXTERNAL_STORAGE,
                                        name));
                                outputStream.write(response);
                                outputStream.close();
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            Log.d("KEY_ERROR", "UNABLE TO DOWNLOAD FILE");
                            e.printStackTrace();
                        }
                    }
                } ,new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO handle the error
                error.printStackTrace();
            }
        }, null);
        RequestQueue mRequestQueue = MyVolley.getRequestQueue();
        mRequestQueue.add(request);
    }
}
